﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    internal class Program
    {
        static void Main(string[] args)
        {

            // Static Classes and Static Class Members
            // https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/static-classes-and-static-class-members

            /*
             * A static class is basically the same as a non-static class, but there is one difference: a static class cannot be instantiated. 
             * In other words, you cannot use the new operator to create a variable of the class type. Because there is no instance variable, 
             * you access the members of a static class by using the class name itself. For example, if you have a static class that is named 
             * UtilityClass that has a public static method named MethodA, you call the method as shown in the following example:
             * UtilityClass.MethodA(); 
             * 
             * A static class can be used as a convenient container for sets of methods that just operate on input parameters and do not have to get or 
             * set any internal instance fields. For example, in the .NET Class Library, the static System.Math class contains methods that perform mathematical operations, 
             * without any requirement to store or retrieve data that is unique to a particular instance of the Math class. That is, you apply the members of the class 
             * by specifying the class name and the method name, as shown in the following example.
             * double dub = -3.14;  
                Console.WriteLine(Math.Abs(dub));  
                Console.WriteLine(Math.Floor(dub));  
                Console.WriteLine(Math.Round(Math.Abs(dub)));  

                 Output:  
                 3.14  
                 -4  
                 3  
             * As is the case with all class types, the type information for a static class is loaded by the .NET runtime when the program that references the class is loaded. 
             * The program cannot specify exactly when the class is loaded. However, it is guaranteed to be loaded and to have its fields initialized and its static constructor 
             * called before the class is referenced for the first time in your program. A static constructor is only called one time, and a static class remains in memory for the 
             * lifetime of the application domain in which your program resides.
             * 
             * The following list provides the main features of a static class:
                Contains only static members.
                Cannot be instantiated.
                Is sealed.
                Cannot contain Instance Constructors.
             */

            // Extension Methods\
            // https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/extension-methods

            /*
             * Extension methods enable you to "add" methods to existing types without creating a new derived type, recompiling, or otherwise modifying the original type. 
             * Extension methods are static methods, but they're called as if they were instance methods on the extended type. For client code written in C#, F# and Visual Basic, 
             * there's no apparent difference between calling an extension method and the methods defined in a type.

             * The most common extension methods are the LINQ standard query operators that add query functionality to the existing System.Collections.IEnumerable and 
             * System.Collections.Generic.IEnumerable<T> types. To use the standard query operators, first bring them into scope with a using System.Linq directive. 
             * Then any type that implements IEnumerable<T> appears to have instance methods such as GroupBy, OrderBy, Average, and so on. You can see these additional methods in 
             * IntelliSense statement completion when you type "dot" after an instance of an IEnumerable<T> type such as List<T> or Array.
             *         
             */
        }
    }
}
