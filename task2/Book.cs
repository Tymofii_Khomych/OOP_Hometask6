﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Book
    {
        public string text = "A static class is basically the same as a non-static class, but there is one difference: a static class cannot be instantiated. " +
            "In other words, you cannot use the new operator to create a variable of the class type. Because there is no instance variable, you access the " +
            "members of a static class by using the class name itself.";
    }
}
