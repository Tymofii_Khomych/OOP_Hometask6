﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    static class FindAndReplaceManager
    {
        public static bool FindNext(this Book book, string str)
        {
            return book.text.Contains(str);
        }
    }
}
