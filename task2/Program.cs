﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book();
            Console.WriteLine(book.FindNext("use the new operator") ? "String was found" : "String was not found");
            Console.WriteLine(book.FindNext("aaa") ? "String was found" : "String was not found");
        }
    }
}
